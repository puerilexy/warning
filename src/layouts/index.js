import styles from './index.css';
import React, { Component } from 'react';
import {
	Layout, Icon, Menu, Input, Avatar, Badge, Popover,
	List, Spin
} from 'antd';
import picHead from '../assets/layouts/imgs/avatar3.jpg';
import waringData from '../../mock/layout/warning';
import siderData from '../../mock/layout/sider';

const { Header, Sider, Content, Footer } = Layout;
const SubMenu = Menu.SubMenu;
const Search = Input.Search;
const IconFont = Icon.createFromIconfontCN({ // 自定义icon：退出登录
	scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
});

const language = [
	{
		imgUrl: require("../assets/global/img/flags/cn.png"),
		languageType: "中文"
	},
	{
		imgUrl: require("../assets/global/img/flags/us.png"),
		languageType: "English"
	},
	{
		imgUrl: require("../assets/global/img/flags/fr.png"),
		languageType: "Franch"
	}
]

class BasicLayout extends Component {
	state = {
		collapsed: false,
		display: true,
		waringData: waringData,
		loading: false,
		hasMore: true,
		clicked: false,
		languageClicked: false,
		nowLanguage: language[0],
		fiflterLanguage: []
	}
	toggle = () => { // 左侧边栏的开关
		this.setState({
			collapsed: !this.state.collapsed,
			display: !this.state.display
		});
	}
	handleClickChange = visible => { // 退出登录气泡的点击事件
		this.setState({
			clicked: visible
		});
	};
	hidePopover = () => { // 点击关闭气泡框
		this.setState({
			clicked: false
		});
	};
	handleLangClickChange = visible => {
		this.setState({
			languageClicked: visible
		});
	}
	handleMenuClick = (e) => { // 点击切换语言
		this.setState({
			nowLanguage: language[e.key * 1],
			languageClicked: false
		})
	};
	render() {
		return (
			<div>
				<Layout>
					{/* 头部 */}
					<Header className={styles.headerWrap} style={{ padding: '0 30px', background: '#526b86' }}>
						{/* 头部左侧logo和toggle按钮 */}
						<div className={styles.headerLeftWrap}>
							<div className={styles.logo} style={{ display: this.state.display ? 'block' : 'none' }} />
							<div className={styles.sidebarToggler}
								type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
								onClick={this.toggle}
							/>
						</div>
						{/* 头部右侧信息 */}
						<div className={styles.headerRightWrap}>
							{/* 头部搜索框 */}
							<div className={styles.searchWrap}>
								<Search
									placeholder="搜索..."
									style={{ width: 170, height: 50 }}
								/>
							</div>
							{/* 头部预警信息 */}
							<div className={styles.msgWrap}>
								<Popover placement="bottomRight"
									trigger="click"
									content={
										<>
											<div className={styles.msgWrapTitle}>
												<h4>7条预警信息</h4>
												<a>查看全部</a>
											</div>
											<div style={{ height: 200, overflow: 'auto' ,paddingTop: '40px'}}>
												<List
													dataSource={this.state.waringData}
													renderItem={item => (
														<List.Item key={item.id}>
															<List.Item.Meta
																avatar={
																	<Avatar icon={item.icon} shape="square" style={{ backgroundColor: item.bgcolor, width: 25, height: 25 }} />
																}
																title={<a href="javascript:;">{item.title}</a>}
															/>
															<div style={{ marginLeft: '15px' }}>
																<a href="javascript:;" style={{ padding: '2px 8px', background: '#f1f1f1', color: '#888' }}>{item.time}</a>
															</div>
														</List.Item>
													)}
												>
													{this.state.loading && this.state.hasMore && (
														<div className="demo-loading-container">
															<Spin />
														</div>
													)}
												</List>
											</div>
										</>
									}
								>
									<span style={{ marginRight: 24 }}>
										<Badge count={7} style={{ backgroundColor: '#17C4BB' }} offset={[-5, 3]}>
											<Avatar style={{ backgroundColor: 'rgba(0,0,0,0)', fontSize: '25px' }} icon="bell" />
										</Badge>
									</span>
								</Popover>
							</div>
							{/* 头部登录用户信息 */}
							<div>
								<Popover placement="bottomRight"
									trigger="click"
									visible={this.state.clicked}
									onVisibleChange={this.handleClickChange}
									content={
										<div style={{ width: '180px', cursor: 'pointer', padding: '12px 16px' }}
											onClick={this.hidePopover}
										>
											<IconFont type="icon-tuichu" />
											<span style={{ marginLeft: '10px' }}>退出登录</span>
										</div>
									}
								>
									<Avatar src={picHead} style={{ margin: '0 15px' }} />
									<span style={{ color: 'rgba(255, 255, 255, 0.65)', marginRight: '15px' }}>
										Nick<Icon type="down" style={{ fontSize: '12px', marginLeft: '8px' }} />
									</span>
								</Popover>
							</div>
							{/* 头部语言切换 */}
							<div>
								<Popover placement="bottomRight"
									trigger="click"
									visible={this.state.languageClicked}
									onVisibleChange={this.handleLangClickChange}
									content={
										<Menu onClick={this.handleMenuClick}>
											{
												language && language.map((item, index) => {
													return <Menu.Item key={index}>
														<a href="javascript:;" style={{ color: '#333', width: '185px', display: 'block' }}>
															<img src={item.imgUrl} alt="error" style={{ margin: '0 8px 0 15px' }} />
															<span>{item.languageType}</span>
														</a>
													</Menu.Item>
												})
											}
										</Menu>
									}
								>
									<div style={{ color: 'rgba(255, 255, 255, 0.65)' }}>
										<img src={this.state.nowLanguage.imgUrl} alt="error" style={{ margin: '0 8px 0 15px' }} />
										<span>{this.state.nowLanguage.languageType}</span>
										<Icon type="down" style={{ fontSize: '12px', margin: '0 8px' }} />
									</div>
								</Popover>
							</div>
						</div>
					</Header>
					<Layout style={{ marginTop: '64px' }}>
						{/* 侧边栏导航 */}
						<Sider trigger={null} collapsible collapsed={this.state.collapsed} style={{ background: '#384a5e' }}>
							<div>
								<Menu defaultSelectedKeys={['1']} mode="vertical" style={{ background: '#384a5e' }}>
									{
										siderData && siderData.map((item,index) => {
											return !item.subMenu ? <Menu.Item key={item.id} className={styles.menuItem}
												style={{height: 80,background:index===0?'#ef505a':null}}
											>
												<Icon type={item.icon} style={{fontSize:'24px'}}/>
												<span>{item.title}</span>
											</Menu.Item> : <SubMenu
												key="sub1"
												style={{height: 80}}
												title={
													<span className={styles.menuItem}>
														<Icon type={item.icon} />
														<span>{item.title}</span>
													</span>
												}
											>
													{
														item.subMenu.map((subItem,index) => {
															return <Menu.Item key={index}>{subItem.title}</Menu.Item>
														})
													}
												</SubMenu>
										})
									}
								</Menu>
							</div>
						</Sider>
						{/* 主要内容 */}
						<Content style={{ height: '600px' }}>
							{this.props.children}
						</Content>
					</Layout>
				</Layout>
				<Layout>
					{/* 底部 */}
					<Footer style={{ background: '#526b86', color: 'rgba(255, 255, 255, 0.65)' }}>
						2019 © zhangshun
						<a href="/" style={{ marginLeft: '15px' }}>突发预警系统</a>
					</Footer>
				</Layout>
			</div >
		);
	}
}

export default BasicLayout;
